"use strict";

const http = require('http');

const log = require('../log');

module.exports = function(port, hostname, message) {

  port = port || 3000;

  hostname = hostname || 'localhost';

  message = message || 'Hello world';

  const server = http.createServer((req, res) => {

    res.statusCode = 200;

    res.setHeader('Content-Type', 'text/plain');

    res.end(message);

    log(message);

  });

  server.listen(port, hostname, () => {

    log(`Server saying '${message}' is running at http://${hostname}:${port}/`);

  });

}
