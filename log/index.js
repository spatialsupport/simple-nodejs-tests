"use strict";

const fs = require('fs')

module.exports = function(msg, logfile) {

  console.log(msg)

  logfile = logfile || 'log.txt';

  const stream = fs.createWriteStream(logfile, {flags:'a'});

  stream.write(`${new Date().toISOString()} > ${msg}\n`, (error) => {

    if (error) throw error;

  });

  stream.end();
};
