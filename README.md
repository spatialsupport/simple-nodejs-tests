# Dead simple NodeJS Webserver Apps #

### Why? ###

Needed some dead simple NodeJS webserver apps to help test a host server 
reverse proxy configuration that was being set by a bash script. Got 
tired of accidentally blasting the simple hello-world.js webserver scripts
used to establish some simple test endpoints for testing. That's why.

Published this repo publicly mainly to make access from the setup script much 
easier. If it's useful to anybody else, great!

### What? ###

* Simple apps for testing
* use basic NodeJS httpServer to set up a simple endpoint
* log to a simple local text file
* no additional node libs required

### Requirements ###

* Vanilla node, built and tested with v14.3.0

### Command line ###

* Apps can use command line args for port and host
    
    `> node ./apps/[app].js [port] [available-hostname]`
    
    defaults are '3000' and 'localhost'

* Examples

        > node ./apps/hi.js
        Server saying 'Hi good lookin' what's cookin'?' is running at http://localhost:3000/

        > node ./apps/bye.js 4000
        Server saying 'Bye bye baby!' is running at http://localhost:4000/

        > node ./apps/json.js 5000 acer
        Server saying '{"hi":"Hi good lookin' what's cookin'?","bye":"Bye bye baby!"}' is running at http://acer:5000/

* There is also a helper app to fork all other apps in the directory

        > node ./apps/all.js 7000
        forking bye.js 7000
        forking hi.js 7001
        forking json.js 7002
        Server saying 'Bye bye baby!' is running at http://localhost:7000/
        Server saying '{"hi":"Hi good lookin' what's cookin'?","bye":"Bye bye baby!"}' is running at http://localhost:7002/
        Server saying 'Hi good lookin' what's cookin'?' is running at http://localhost:7001/

* Log filename can be changed, but default is log.txt in the parent directory

        2020-08-15T03:17:46.385Z > Server saying 'Bye bye baby!' is running at http://localhost:7000/
        2020-08-15T03:17:46.399Z > Server saying 'Hi good lookin' what's cookin'?' is running at http://localhost:7001/
        2020-08-15T03:17:46.412Z > Server saying '{"hi":"Hi good lookin' what's cookin'?","bye":"Bye bye baby!"}' is running at http://localhost:7002/
        2020-08-15T03:17:57.102Z > Bye bye baby!
        2020-08-15T03:18:01.223Z > Hi good lookin' what's cookin'?
        2020-08-15T03:18:04.837Z > {"hi":"Hi good lookin' what's cookin'?","bye":"Bye bye baby!"}
        2020-08-15T03:18:13.207Z > Hi good lookin' what's cookin'?
        2020-08-15T03:18:14.150Z > {"hi":"Hi good lookin' what's cookin'?","bye":"Bye bye baby!"}
        2020-08-15T03:18:15.440Z > {"hi":"Hi good lookin' what's cookin'?","bye":"Bye bye baby!"}
        2020-08-15T03:18:18.096Z > Bye bye baby!
        2020-08-15T03:18:19.996Z > Hi good lookin' what's cookin'?

### Some bash that will download and clean up ###

    RELEASE="blackdog"
    TARFILE="$RELEASE.tar.gz"
    URL="https://bitbucket.org/spatialsupport/simple-nodejs-tests/get/release/$TARFILE"
    rm -f $TARFILE && wget $URL
    rm -rf $RELEASE && mkdir $RELEASE
    tar -xvzf $TARFILE -C $RELEASE --strip-components=1 && rm $TARFILE

*in memory of my beautiful black dog, Noi*
