"use strict";

const path = require('path')

const fs = require('fs')

const args = process.argv.slice(2);

const startPort = parseInt(args[0]) || 3000;

const host = args[1] || 'localhost';

function filesInPath(inPath, filter){

  const files = fs.readdirSync(inPath);

  let matches = [];

  for(let i = 0; i < files.length; i++){

    const filename = path.join(inPath, files[i]);

    if (filename === __filename) continue; // I skip myself

    const stat = fs.lstatSync(filename);

    if (stat.isDirectory()){

      filesInPath(filename,filter); // recurse
    }
    else if (filename.indexOf(filter) >= 0) {

      matches.push(filename);

    };
  };

  return matches;
};

const apps = filesInPath(__dirname,'.js');

for(let i = 0; i < apps.length; i++){

  console.log('spawning', path.basename(apps[i]), startPort + i)

  const spawn = require('child_process').spawn;

  let child = spawn(process.argv[0], [apps[i], startPort + i, host], {
    detached: true,
    stdio: ['ignore']
  });

  child.unref();

  child = null;

};

console.log('done')

